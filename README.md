# Lekcja 1
## Zadanie 1 - basic 
Zadeklaruj i zainicjalizuj zmienne dowolnymi wartościami wg poniższego schematu:

* zmienna `number` powinna być typu `Number`
* zmienna `text` powinna być typu `String`
* zmienna `logic` powinna być typu `Boolean`
* zmienna `empty` powinna być wartością pustą (`null`) lub niezainicjalizowana
* zmienna `args` powinna być typu `Array`
