const {
  number,
  text,
  logic,
  empty,
  args,
} = require('..');

describe('Variable', () => {
  it('number should be a number', async () => {
    expect(typeof number).toBe('number');
  });

  it('text should be a string', async () => {
    expect(typeof text).toBe('string');
  });

  it('logic should be a boolean', async () => {
    expect(typeof logic).toBe('boolean');
  });

  it('empty should be an undefined or null', async () => {
    expect(empty === undefined || empty === null).toBeTruthy();
  });

  it('args should be an array', async () => {
    expect(Array.isArray(args)).toBeTruthy();
  });
});
